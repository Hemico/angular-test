// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyDqZjul9xSYJK3lUApkvIT6F1kF9YjW9D4",
    authDomain: "angular-hemi.firebaseapp.com",
    databaseURL: "https://angular-hemi.firebaseio.com",
    projectId: "angular-hemi",
    storageBucket: "angular-hemi.appspot.com",
    messagingSenderId: "947051950482"
  }
};